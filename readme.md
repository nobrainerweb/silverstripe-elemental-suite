# Nobrainer Web - Content Block Suite
Based on the SilverStripe Elemental module for making content blocks.
This library seeks to streamline an opinionated set of content blocks, 
with which the user has more features and freedom to create what they desire.


## Caveats
When installing the module, it might be necessary to declare module priority
for the project module (typically named "app"), which can be done like so:

```
SilverStripe\Core\Manifest\ModuleManifest:
  project: app
  module_priority:
    - vendor/app
```
