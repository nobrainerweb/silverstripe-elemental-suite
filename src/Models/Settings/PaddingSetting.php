<?php

namespace Nobrainer\Elemental\Settings;

use Nobrainer\Elemental\Core\ElementSettingExtension;
use Nobrainer\Elemental\Core\JSONText;
use SilverStripe\Forms\DropdownField;

class PaddingSetting extends ElementSettingExtension
{
    private static $settingTab = 'Block';
    private static $column = 'Padding';
    private static $defaultValue = 'none';
    private static $cssBase = 'padding';
    private static $excludedValues = 'none';

    private static $db = [
        'Padding' => JSONText::class
    ];

    private static $settings = [
        'Top'    => DropdownField::class,
        'Right'  => DropdownField::class,
        'Bottom' => DropdownField::class,
        'Left'   => DropdownField::class,
    ];

    protected function getSourceFor($setting): array
    {
        return [
            'none'   => 'None',
            'small'  => 'Small',
            'medium' => 'Medium',
            'large'  => 'Large',
        ];
    }

    public function Padding()
    {
        return $this->prepareTemplateData();
    }
}
