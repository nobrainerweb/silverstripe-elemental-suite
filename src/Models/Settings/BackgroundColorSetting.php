<?php

namespace Nobrainer\Elemental\Settings;

use Nobrainer\Elemental\Core\ElementSettingExtension;
use RyanPotter\SilverStripeColorField\Forms\ColorField;
use SilverStripe\ORM\FieldType\DBVarchar;

class BackgroundColorSetting extends ElementSettingExtension
{
    private static $title = 'Background Color';
    private static $column = 'BackgroundColor';
    private static $cssBase = 'background-color';
    private static $defaultValue = 'none';

    private static $db = [
        'BackgroundColor' => 'Text',
    ];

    private static $settings = [
        'BackgroundColor' => [
            'field' => ColorField::class,
            'label' => 'Color'
        ]
    ];

    public function getColor()
    {
        return $this->getColumnValue();
    }

    public function hasColor()
    {
        $color = $this->getColor();

        return $color !== $this->getDefaultValue() && preg_match('/^#/', $color);
    }

    public function BackgroundColor()
    {
        $style = $this->hasColor() ? "background-color: {$this->getColor()};" : '';

        return $this->prepareTemplateData(['Style' => $style]);
    }

    protected function toMeString()
    {
        if ($this->hasColor()) {
            $base = $this->getCssBase();

            return "with-$base";
        }

        return '';
    }
}