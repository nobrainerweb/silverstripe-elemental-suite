<?php

namespace Nobrainer\Elemental\Settings;

use Nobrainer\Elemental\Core\ElementSettingExtension;
use Nobrainer\Elemental\Core\JSONText;
use SilverStripe\Forms\DropdownField;

class TextAlignmentSetting extends ElementSettingExtension
{
    private static $title = 'Text Alignment';
    private static $column = 'TextAlignment';
    private static $cssBase = '';
    private static $defaultValue = 'left';
    private static $db = [
        'TextAlignment' => JSONText::class
    ];

    private static $settings = [
        'TextAlignment' => [
            'field' => DropdownField::class,
            'label' => 'Text Alignment'
        ],
    ];

    protected function getSourceFor($setting): array
    {
        return [
            'left'    => 'Left',
            'center'  => 'Center',
            'right'   => 'Right',
            'justify' => 'Justify',
        ];
    }

    public function TextAlignment()
    {
        return $this->prepareTemplateData();
    }
}