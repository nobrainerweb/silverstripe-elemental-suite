<?php

namespace Nobrainer\Elemental\Settings;

use Nobrainer\Elemental\Core\ElementSettingExtension;
use Nobrainer\Elemental\Core\JSONText;
use SilverStripe\Forms\DropdownField;

class MarginSetting extends ElementSettingExtension
{
    private static $settingTab = 'Block';
    private static $column = 'Margin';
    private static $cssBase = 'margin';
    private static $excludedValues = 'none';

    private static $db = [
        'Margin' => JSONText::class
    ];

    private static $defaultValue = ['Bottom' => 'medium'];

    private static $settings = [
        'Top'    => DropdownField::class,
        'Right'  => DropdownField::class,
        'Bottom' => DropdownField::class,
        'Left'   => DropdownField::class,
    ];

    protected function getSourceFor($setting): array
    {
        return [
            'none'   => 'None',
            'small'  => 'Small',
            'medium' => 'Medium',
            'large'  => 'Large',
        ];
    }

    public function Margin()
    {
        return $this->prepareTemplateData();
    }
}
