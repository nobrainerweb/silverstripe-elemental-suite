<?php

namespace Nobrainer\Elemental\Settings;

use Nobrainer\Elemental\Core\ElementSettingExtension;
use Nobrainer\Elemental\Core\JSONText;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\DropdownField;
use SilverStripe\ORM\FieldType\DBField;
use SilverStripe\ORM\FieldType\DBHTMLText;
use SilverStripe\ORM\FieldType\DBHTMLVarchar;
use SilverStripe\View\Parsers\ShortcodeParser;

class ImageResamplingSetting extends ElementSettingExtension
{
    private static $title = 'Image Resampling';
    private static $column = 'ImageResampling';
    private static $defaultValue = 'Fill';
    private static $placeholderRatio = 100;
    private static $minPlaceholderSize = 4;
    private static $db = [
        'ImageResampling' => JSONText::class
    ];

    private static $settings = [
        'Method' => [
            'field'  => DropdownField::class,
            'source' => [
                'Fill'           => 'Fyld',
                'Fit'            => 'Tilpas',
                'Pad'            => 'Tilpas (Udfyld)',
                'ScaleWidth'     => 'Fuld højde',
                'ScaleHeight'    => 'Skaler højde',
                'ScaleMaxHeight' => 'Skaler max-højde',
            ]
        ],
    ];

    /**
     * Resample the images as specified.
     *
     * The ID is used to get the image, since it is not possible
     * to pass an entire image object to the function in the template.
     *
     * @param     $id
     * @param int $width
     * @param int $height
     * @return mixed
     */
    public function ResampledImage($id, $width = 300, $height = 200)
    {
        $image = $this->findImage($id);
        if (!$image->exists()) {
            return;
        }
        $object = $this->getColumnObject();
        $method = $object->Method;

        switch ($method) {
            case 'Fill':
            case 'Fit':
            case 'Pad':
            case 'ScaleWidth':
                return $image->$method($width, $height);

            case 'ScaleHeight':
            case 'ScaleMaxHeight':
                return $image->$method($height);

            default:
                $defaultMethod = $this->getDefaultValue();

                return $image->{$defaultMethod}($width, $height);
        }
    }

    /**
     * Resample the image as a lazy-loading placeholder,
     * such that it is as small as possible to load quickly,
     * while still retaining a smidge of the image's palette.
     *
     * @param     $id
     * @param int $width
     * @param int $height
     * @return mixed
     */
    public function PlaceholderImage($id, $width = 300, $height = 200)
    {
        return $this->ResampledImage($id, $this->resize($width), $this->resize($height));
    }

    /**
     * Scale the image to the given size using the configured ratio.
     *
     * @param $size
     * @return mixed
     */
    public function resize($size)
    {
        $ratio = self::config()->get('placeholderRatio');
        $min = self::config()->get('minPlaceholderSize');

        return max($size / $ratio, 0.1);
    }

    /**
     * Render an image with the same template used in .ss files.
     * The use-case for this function is when outputting images in a loop,
     * since doing so in the template forfeits some of the context,
     * i.e. dropping $Height and $Width values from the scope.
     *
     * Typically, this function will be called as such:
     *      $Up.RenderImage($ID, $Width, $Height)
     *
     * @param $id
     * @param $width
     * @param $height
     * @return DBHTMLText
     */
    public function RenderImage($id, $width, $height): DBHTMLText
    {
        $image = $this->findImage($id);

        $html = $this->owner->customise([
            'Image'  => $image,
            'Width'  => $width,
            'Height' => $height,
        ])->renderWith('Nobrainer\Elemental\Includes\Image');

        return DBHTMLText::create()->setValue($html);
    }

    /**
     * Semi-reliably find the given image or a reasonable substitute.
     * An empty image is returned if there is no image.
     *
     * @param $id
     * @return Image
     */
    public function findImage($id): Image
    {
        if ($id) {
            if ($image = Image::get_by_id($id)) {
                return $image;
            }
        } elseif ($image = $this->owner->Images()->first()) {
            return $image;
        }

        return Image::create();
    }

}