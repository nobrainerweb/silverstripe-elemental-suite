<?php

namespace Nobrainer\Elemental\Settings;

use Nobrainer\Elemental\Core\ElementSettingExtension;
use Nobrainer\Elemental\Core\JSONText;
use SilverStripe\Forms\OptionsetField;

class ImageAlignmentSetting extends ElementSettingExtension
{
    private static $title = 'Image Alignment';
    private static $column = 'ImageAlignment';
    private static $defaultValue = 'left';

    private static $db = [
        'ImageAlignment' => JSONText::class
    ];

    private static $settings = [
        'ImageAlignment' => OptionsetField::class,
    ];

    protected function getSourceFor($setting): array
    {
        return [
            'left'   => 'Left',
            'right'  => 'Right',
        ];
    }

    public function ImageAlignment()
    {
        return $this->prepareTemplateData();
    }
}