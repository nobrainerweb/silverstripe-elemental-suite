<?php

namespace Nobrainer\Elemental\Settings;

use Nobrainer\Elemental\Core\JSONText;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\DropdownField;

class _StencilSetting extends ElementSettingExtension
{
    private static $column = 'Stencil';
    private static $defaultValue = 'default';
    private static $db = [
        'Stencil' => JSONText::class
    ];

    private static $settings = [
        'Setting1' => TextField::class,
        'Setting2' => [
            'field'  => DropdownField::class,
            'label'  => 'Setting Label',
            'source' => [
                'foo' => 'bar',
                'baz' => 'qux',
            ]
        ],
    ];

    public function Stencil()
    {
        return $this->prepareTemplateData();
    }
}