<?php

namespace Nobrainer\Elemental\Settings;

use Nobrainer\Elemental\Core\ElementSettingExtension;
use Nobrainer\Elemental\Core\JSONText;
use SilverStripe\Forms\DropdownField;

class WidthSetting extends ElementSettingExtension
{
    private static $column = 'Width';
    private static $defaultValue = 'default';

    private static $db = [
        'Width' => JSONText::class
    ];

    private static $settings = [
        'Width' => [
            'field'  => DropdownField::class,
            'source' => [
                'full'  => 'Full Width',
                'box'   => 'Boxed',
                'fluid' => 'Fluid',
            ]
        ]
    ];

    public function Width()
    {
        return $this->prepareTemplateData();
    }
}