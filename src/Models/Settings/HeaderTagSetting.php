<?php

namespace Nobrainer\Elemental\Settings;

use Nobrainer\Elemental\Core\ElementSettingExtension;
use Nobrainer\Elemental\Core\JSONText;
use SilverStripe\Forms\DropdownField;

class HeaderTagSetting extends ElementSettingExtension
{
    private static $title = 'Header Tag';
    private static $column = 'HeaderTag';
    private static $defaultValue = [
        'Tag'          => 'h2',
        'UseHeaderTag' => 'none'
    ];

    private static $db = [
        'HeaderTag' => JSONText::class
    ];

    private static $settings = [
        'Tag'          => [
            'field'  => DropdownField::class,
            'source' => [
                'h1' => 'h1',
                'h2' => 'h2',
                'h3' => 'h3',
                'h4' => 'h4',
                'h5' => 'h5',
                'h6' => 'h6',
            ]
        ],
        'UseHeaderTag' => [
            'field'  => DropdownField::class,
            'label'  => 'Use Header Tag',
            'source' => [
                'none' => 'Ingen tag',
                'tag'  => 'Brug tag'
            ]
        ]
    ];

    public function HeaderTag()
    {
        return $this->prepareTemplateData();
    }
}