<?php

namespace Nobrainer\Elemental\Settings;

use Nobrainer\Elemental\Core\ElementSettingExtension;
use SilverStripe\Forms\DropdownField;

class ContentColumnSetting extends ElementSettingExtension
{
    private static $column = 'Columns';
    private static $cssBase = '';
    private static $defaultValue = 1;
    private static $db = [
        'Columns' => 'Text'
    ];

    private static $settings = [
        'Columns' => [
            'field'  => DropdownField::class,
            'source' => [
                '1' => 'No columns',
                '2' => '2',
                '3' => '3',
            ]
        ],
    ];

    public function Columns()
    {
        return $this->prepareTemplateData();
    }
}