<?php

namespace Nobrainer\Elemental\Settings;

use Nobrainer\Elemental\Core\ElementSettingExtension;
use Nobrainer\Elemental\Core\JSONText;
use SilverStripe\Forms\DropdownField;

class ListColumnCountSetting extends ElementSettingExtension
{
    private static $title = 'Number of List Columns';
    private static $column = 'ListColumnCount';
    private static $cssBase = '';
    private static $defaultValue = '4';
    private static $db = [
        'ListColumnCount' => JSONText::class
    ];

    private static $settings = [
        'ListColumnCount' => [
            'field' => DropdownField::class,
            'label' => 'Number of List Columns (when shown as grid only)'
        ],
    ];

    protected function getSourceFor($setting): array
    {
        return [
            '6'  => '2',
            '4'  => '3',
            '3'  => '4',
        ];
    }

    public function ListColumnCount()
    {
        return $this->prepareTemplateData();
    }
}