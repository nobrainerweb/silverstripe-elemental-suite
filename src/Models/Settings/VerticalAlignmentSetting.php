<?php

namespace Nobrainer\Elemental\Settings;

use Nobrainer\Elemental\Core\ElementSettingExtension;
use Nobrainer\Elemental\Core\JSONText;
use SilverStripe\Forms\DropdownField;

class VerticalAlignmentSetting extends ElementSettingExtension
{
    private static $title = 'Vertical Alignment';
    private static $column = 'VerticalAlignment';
    private static $cssBase = '';
    private static $defaultValue = 'top';
    private static $db = [
        'VerticalAlignment' => JSONText::class
    ];

    private static $settings = [
        'VerticalAlignment' => [
            'field' => DropdownField::class,
            'label' => 'Vertical Alignment'
        ],
    ];

    protected function getSourceFor($setting): array
    {
        return [
            'top'    => 'Top',
            'middle'  => 'Middle',
            'bottom'   => 'Bottom',
        ];
    }

    public function VerticalAlignment()
    {
        return $this->prepareTemplateData();
    }
}