<?php

namespace Nobrainer\Elemental\Elements;

use Sheadawson\Linkable\Forms\LinkField;
use Sheadawson\Linkable\Models\Link;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;

/**
 * Created by PhpStorm.
 * User: tn
 * Date: 30/03/2020
 * Time: 17.04
 */
class ColumnElement extends ContentElement
{
    private static $table_name = 'NobrainerColumnElement';
    private static $singular_name = 'Columns element';
    private static $plural_name = 'Column element';

    private static $description = 'A basic element for content rendered in columns via different templates';
    private static $icon = 'font-icon-block-layout';

    /**
     * @var array
     */
    private static $db = [
        'Text1' => 'HTMLText',
        'Text2' => 'HTMLText',
        'Text3' => 'HTMLText',
        'Text4' => 'HTMLText',
    ];

    /**
     * @var array
     */
    private static $has_one = [
        'Image1' => Image::class,
        'Image2' => Image::class,
        'Image3' => Image::class,
        'Image4' => Image::class,
        'Link1'  => Link::class,
        'Link2'  => Link::class,
        'Link3'  => Link::class,
        'Link4'  => Link::class
    ];

    private static $owns = [
        'Image1',
        'Image2',
        'Image3',
        'Image4',
        'Link1',
        'Link2',
        'Link3',
        'Link4'
    ];

    public function getCMSFields()
    {
        $this->beforeUpdateCMSFields(function (FieldList $fields) {
            for ($i = 1; $i <= 4; $i++) {
                $fields->removeByName(["Image{$i}", "Text{$i}", "Link{$i}ID"]);
                $fields->addFieldsToTab('Root.Unused.ColumnsTabset.Col' . $i, [
                    UploadField::create("Image{$i}", "Image {$i}"),
                    HTMLEditorField::create("Text{$i}", "Text {$i}"),
                    LinkField::create("Link{$i}ID", "Link {$i}", $this)
                ]);
            }
            $this->addTemplateSpecificFields($fields);

            // Remove link and file tracking tabs
            $fields->removeByName(['Unused']);
        });

        return parent::getCMSFields();
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return _t(__CLASS__ . '.BlockType', 'Columns');
    }
}