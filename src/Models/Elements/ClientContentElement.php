<?php

namespace Nobrainer\Elemental\Elements;

use DNADesign\Elemental\Forms\TextCheckboxGroupField;
use Sheadawson\Linkable\Forms\LinkField;
use Sheadawson\Linkable\Models\Link;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataObject;

class ClientContentElement extends DataObject
{
    private static $table_name = 'NobrainerClientContentElement';
    private static $singular_name = 'Client element';
    private static $plural_name = 'Client elements';

    private static $description = 'A content element used to represent a client with a name and a logo';
    private static $icon = 'font-icon-torso';

    private static $db = [
        'Sort'  => 'Int',
        'Title' => 'Varchar',
        'Website'  => 'Varchar(200)'
    ];

    private static $has_one = [
        'ElementList' => ClientListElement::class,
        'Logo'        => Image::class
    ];

    private static $owns = [
        'Logo'
    ];

    /**
     * Re-title the HTML field to Content
     *
     * {@inheritDoc}
     */
    public function getCMSFields()
    {

        $fields = parent::getCMSFields();

        $fields->removeByName(['Sort', 'ElementListID']);

        return $fields;
    }
}
