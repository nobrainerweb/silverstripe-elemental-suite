<?php

namespace Nobrainer\Elemental\Elements;

class FeatureElement extends ContentElement
{
    private static $table_name = 'NobrainerFeatureElement';
    private static $singular_name = 'Feature element';
    private static $plural_name = 'Feature element';

    private static $description = 'A basic element for content rendered in columns via different templates';
    private static $icon = 'font-icon-block-layout';

    /**
     * @return string
     */
    public function getType(): string
    {
        return _t(__CLASS__ . '.BlockType', 'Featured content');
    }
}