<?php

namespace Nobrainer\Elemental\Elements;

class CtaElement extends ContentElement
{
    private static $table_name = 'NobrainerCtaElement';
    private static $singular_name = 'Call to Action Element';
    private static $plural_name = 'Call to Action Elements';

    private static $description = 'Call to Action';
    private static $icon = 'font-icon-clipboard-pencil';

    public function getType()
    {
        return _t(__CLASS__ . '.BlockType', 'CTA');
    }
}
