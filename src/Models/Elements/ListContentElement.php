<?php

namespace Nobrainer\Elemental\Elements;

use DNADesign\Elemental\Forms\TextCheckboxGroupField;
use Sheadawson\Linkable\Forms\LinkField;
use Sheadawson\Linkable\Models\Link;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataObject;

class ListContentElement extends CoreElement
{
    private static $table_name = 'NobrainerListContentElement';
    private static $singular_name = 'List Content Element';
    private static $plural_name = 'List Content Elements';

    private static $description = 'A content element used as list items under a list element';
    private static $icon = 'font-icon-clipboard-pencil';

    private static $has_one = [
        'ElementList' => ListElement::class
    ];

    /**
     * Re-title the HTML field to Content
     *
     * {@inheritDoc}
     */
    public function getCMSFields()
    {

        $fields = parent::getCMSFields();

        $fields->removeByName(['ElementListID']);

        return $fields;
    }
}
