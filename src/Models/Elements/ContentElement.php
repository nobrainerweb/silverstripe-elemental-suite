<?php


namespace Nobrainer\Elemental\Elements;


class ContentElement extends CoreElement
{
    private static $table_name = 'NobrainerContentElement';
    private static $singular_name = 'Basic text content element';
    private static $plural_name = 'Basic text content elements';

    private static $description = 'A basic element for content';
}