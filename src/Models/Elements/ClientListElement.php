<?php

namespace Nobrainer\Elemental\Elements;

use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\GridField\GridFieldConfig;
use SilverStripe\View\Requirements;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;

class ClientListElement extends ContentElement
{
    private static $table_name = 'NobrainerClientListElement';
    private static $singular_name = 'Client list element';
    private static $plural_name = 'Client list element';

    private static $description = 'An element for showing a list of client logoes';
    private static $icon = 'font-icon-thumbnails';

    /**
     * @var array
     */
    private static $has_many = [
        'Elements' => ClientContentElement::class
    ];

    private static $owns = [
        'Elements'
    ];

    private static $cascade_deletes = [
        'Elements'
    ];

    private static $cascade_duplicates = [
        'Elements'
    ];

    /**
     * @return string
     */
    public function getType(): string
    {
        return _t(__CLASS__ . '.BlockType', 'Clients');
    }

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        if (!$this->Style) {
            $fields->removeByName('Elements');
        } elseif ($this->isInDB()) {
            /** @var GridFieldConfig $config */
            $config = $fields->dataFieldByName('Elements')->getConfig();
            $config->addComponent(GridFieldOrderableRows::create('Sort'));
            $this->applyElementalGridFix();
        }

        return $fields;
    }

    /**
     * Due to a bug in Elemental (most likely), the nested Element grid
     * has <a> tags with empty [href] attributes, which cause clicking the
     * list item to link to the website index ("/"), leaving the CMS.
     *
     * This javascript injection removes the empty href attributes.
     */
    public function applyElementalGridFix()
    {
        Requirements::customScript(<<< JS
            var elements = document.querySelectorAll('a.elemental-edit');
            for (var i in elements) elements[i].removeAttribute('href');
        JS);
    }
}