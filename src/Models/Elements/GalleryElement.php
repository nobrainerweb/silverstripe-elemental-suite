<?php

namespace Nobrainer\Elemental\Elements;

class GalleryElement extends ContentElement
{

    private static $table_name = 'NobrainerGalleryElement';
    private static $singular_name = 'Image Gallery Emelent';
    private static $plural_name = 'Image Gallery Emelents';

    private static $description = 'A element for creating image gallery';
    private static $icon = 'font-icon-picture';

    public function getType()
    {
        return _t(__CLASS__ . '.BlockType', 'Gallery');
    }
    
    
}
