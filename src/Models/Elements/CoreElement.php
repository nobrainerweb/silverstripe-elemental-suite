<?php

namespace Nobrainer\Elemental\Elements;

use Bummzack\SortableFile\Forms\SortableUploadField;
use DNADesign\Elemental\Models\BaseElement;
use Sheadawson\Linkable\Forms\LinkField;
use Sheadawson\Linkable\Models\Link;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\Image;
use SilverStripe\Control\Director;
use SilverStripe\Core\ClassInfo;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldAddExistingAutocompleter;
use SilverStripe\Forms\GridField\GridFieldConfig;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
use SilverStripe\Forms\GridField\GridFieldFilterHeader;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\ReadonlyField;
use SilverStripe\Forms\Tab;
use SilverStripe\Forms\TabSet;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataObject;
use SilverStripe\ORM\FieldType\DBField;
use SilverStripe\ORM\Queries\SQLDelete;
use SilverStripe\Versioned\Versioned;
use SilverStripe\View\Requirements;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;
use TractorCow\Fluent\Model\Locale;

class CoreElement extends BaseElement
{
    private static $table_name = 'NobrainerCoreElement';

    private static $icon = 'font-icon-block-content';

    private static $inline_editable = false;

    private static $db = [
        'Tagline' => 'Varchar(255)',
        'Content' => 'HTMLText'
    ];

    /**
     * @var array
     */
    private static $has_one = [
        'MediaLink' => Link::class
    ];

    /**
     * @var array
     */
    private static $many_many = [
        'Images' => Image::class,
        'Links'  => Link::class,
    ];

    private static $many_many_extraFields = [
        'Images' => ['Sort' => 'Int'],
        'Links'  => ['Sort' => 'Int']
    ];

    /**
     * @var array
     */
    private static $owns = [
        'Images'
    ];

    /**
     * Re-title the HTML field to Content
     *
     * {@inheritDoc}
     */
    public function getCMSFields()
    {
        $this->beforeUpdateCMSFields(function (FieldList $fields) {
            /** @var HTMLEditorField $editorField */
            $editorField = $fields->fieldByName('Root.Main.Content');
            $editorField->setTitle(_t(__CLASS__ . '.ContentLabel', 'Content'));
            $fields->addFieldToTab('Root.Main', TextField::create('Tagline', _t(__CLASS__ . '.TaglineLabel', 'Tagline')), 'Content');

            $fields->removeByName(['Settings', 'MediaLinkID', 'Links']);

            if ($this->isInDB()) {
                $config = GridFieldConfig_RecordEditor::create()
                    ->addComponent(GridFieldOrderableRows::create())
                    ->removeComponentsByType([GridFieldFilterHeader::class]);
                $fields->addFieldToTab('Root.Main', GridField::create('Links', 'Links', $this->Links(), $config));
            } else {
                $fields->addFieldToTab('Root.Main', LiteralField::create('LinksSaveInfo', 'You can add links after saving'));
            }

            $fields->addFieldToTab('Root.Images', SortableUploadField::create('Images')->setSortColumn('Sort'));
            $fields->addFieldToTab('Root.Images', LinkField::create('MediaLinkID', 'Media Link', $this)
                ->setDescription('If more than 1 image is used, the Media Link is disabled.'));

            // If no template selected, only allow template selection
            if ($this->hasTemplates()) {
                $fields->removeByName(['Main', 'Images', 'Settings', 'History', 'Styling']);
                $this->addTemplateFields($fields);
            } else {
                $this->addTemplateSpecificFields($fields);
                $this->addTemplateFields($fields);
            }
        });

        Requirements::css('nobrainer/silverstripe-elemental-suite:client/dist/css/cms.css');

        $fields = parent::getCMSFields();

        if (!$this->Style) {
            // If no template is selected, force only template Style selection,
            // provided there is actually a template style field.
            if ($template = $fields->dataFieldByName('Style')) {
                // Must add Root TabSet with a Tab, since Elemental forces
                // the resulting Fieldset to deliver a TabSet called "Root".
                $fields = FieldList::create(TabSet::create('Root', Tab::create('Template', $template)));
            }
        }

        return $fields;
    }

    public function populateDefaults()
    {
        $templates = $this->getTemplates();
        if (count($templates) === 1) {
            $this->Style = array_keys($templates)[0];
        }

        return parent::populateDefaults();
    }

    /**
     * This override is not used for anything in particular,
     * but ensures missing methods on settings does not cause
     * breakage.
     * This is especially useful when a specific element has a
     * setting that other blocks in the same list does not.
     *
     * @param string $method
     * @param array  $arguments
     * @return mixed|string
     */
    public function __call($method, $arguments)
    {
        if (!$this->hasMethod($method)) {
            return '';
        }

        return parent::__call($method, $arguments);
    }

    protected function hasTemplates()
    {
        $styles = $this->getTemplateStyles();

        return count($styles) > 0 && !$this->Style;
    }

    private function addTemplateFields(FieldList $fields): void
    {
        $styles = $this->getTemplateStyles();
        if ($styles && count($styles) > 0) {

            $fields->addFieldsToTab('Root.Template', [
                DropdownField::create('Style', 'Template', $styles)
                    ->setEmptyString(_t(__CLASS__ . '.CUSTOM_STYLES', 'Select a template'))
            ]);

        } else {
            $fields->removeByName('Style');
        }
    }

    protected function addTemplateSpecificFields(FieldList $fields): void
    {
        $style_settings = $this->getSelectedTemplateSettings();
        $settingsFields = $style_settings['fields'] ?? [];

        foreach ($settingsFields as $column => $elm_fields) {
            $data_fields = [];
            foreach ($elm_fields as $fieldname) {
                $data_fields[] = $fields->dataFieldByName($fieldname);

            }

            $fields->addFieldsToTab("Root.$column", $data_fields);
        }
    }

    public function getSelectedTemplateSettings()
    {
        $settings = $this->config()->get('styles');

        return $settings[$this->Style] ?? [];
    }

    public function getTemplateStyles()
    {
        $styles = $this->getTemplates();
        $styles_flat = array();

        foreach ($styles as $key => $obj) {
            $styles_flat[$key] = $obj['label'];
        }

        return $styles_flat;
    }

    public function getTemplates()
    {
        return static::config()->uninherited('styles') ?? [];
    }

    public function getSummary()
    {
        return DBField::create_field('HTMLText', $this->Content)->Summary(20);
    }

    protected function provideBlockSchema()
    {
        $blockSchema = parent::provideBlockSchema();
        $blockSchema['content'] = $this->getSummary();

        return $blockSchema;
    }

    public function getType()
    {
        return _t(__CLASS__ . '.BlockType', 'Content');
    }

    public function isLive()
    {
        return Director::isLive();
    }

    public function getUnsetTemplateMessage(): string
    {
        $class = ClassInfo::shortName($this->ClassName);

        return "Fallback template for *$class*. No specific style has been selected!";
    }

    public function MainImage()
    {
        return $this->Images()->Sort('Sort')->First();
    }

    public function isYoutubeMedia()
    {
        return preg_match('/youtu\.?be/', $this->MediaLink()->URL);
    }
}
