<?php

namespace Nobrainer\Elemental\Core;

use SilverStripe\ORM\FieldType\DBText;

/**
 * This class is only meant to deter Fluent from translating
 * settings objects with arbitrary JSON in them.
 *
 * Class JSONText
 * @package Nobrainer\Elemental\Core
 */
class JSONText extends DBText
{
    
}