<?php

namespace Nobrainer\Elemental\Core;

use SilverStripe\View\ArrayData;

/**
 * Enable ArrayData to render itself by mocking $Me with the default value.
 * This way, we can output anything without a forTemplate directly,
 * while maintaining desired aggregates for the object.
 *
 * Class TemplateData
 * @package NobrainerWeb\Elemental
 */
class TemplateData extends ArrayData
{
    public function setMe($value): void
    {
        $this->setField('Me', $value);
    }

    public function forTemplate()
    {
        return $this->getField('Me');
    }
}
