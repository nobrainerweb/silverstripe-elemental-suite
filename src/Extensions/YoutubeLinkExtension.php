<?php
/**
 * Created by PhpStorm.
 * User: tn
 * Date: 17/11/2020
 * Time: 10.18
 */

namespace Nobrainer\Elemental\Extensions;

use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataExtension;

class YoutubeLinkExtension extends DataExtension
{
    private static $db = [
        'YoutubeVideo' => 'Varchar(20)',
    ];

    private static $types = [
        'YoutubeVideo' => 'YouTube video',
    ];

    public function updateCMSFields(FieldList $fields)
    {
        $fields->addFieldToTab('Root.Main',
            TextField::create('YoutubeVideo', 'Youtube Video Code')
                ->setDescription('The video code can be found here: https://www.youtube.com/watch?v=<strong>dQw4w9WgXcQ</strong>')
                ->displayIf('Type')->isEqualTo('YoutubeVideo')->end()
        );
    }

    public function updateLinkURL(&$url)
    {
        $owner = $this->owner;
        if ($owner->Type == 'YoutubeVideo') {
            $url = 'https://www.youtube.com/embed/' . $owner->YoutubeVideo;
        }
    }
}