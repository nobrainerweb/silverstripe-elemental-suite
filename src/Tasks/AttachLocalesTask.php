<?php

namespace Nobrainer\Elemental\Tasks;

use Nobrainer\Elemental\Elements\CoreElement;
use SilverStripe\Dev\BuildTask;
use TractorCow\Fluent\Model\Locale;

class AttachLocalesTask extends BuildTask
{
    public function run($request)
    {
        $elements = CoreElement::get();
        $locales = Locale::get();

        foreach ($elements as $element) {
            foreach ($locales as $locale) {
                $element->FilteredLocales()->add($locale);
            }
        }
    }
}